"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _semanticUiReact = require("semantic-ui-react");

require("semantic-ui-css/semantic.min.css");

require("./style.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Timeline = /*#__PURE__*/function (_Component) {
  _inherits(Timeline, _Component);

  var _super = _createSuper(Timeline);

  function Timeline(props, context) {
    var _this;

    _classCallCheck(this, Timeline);

    _this = _super.call(this, props, context);

    var generateUUID = function generateUUID() {
      // Public Domain/MIT
      var d = new Date().getTime(); //Timestamp

      var d2 = performance && performance.now && performance.now() * 1000 || 0; //Time in microseconds since page-load or 0 if unsupported

      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16; //random number between 0 and 16

        if (d > 0) {
          //Use timestamp until depleted
          r = (d + r) % 16 | 0;
          d = Math.floor(d / 16);
        } else {
          //Use microseconds since page-load if supported
          r = (d2 + r) % 16 | 0;
          d2 = Math.floor(d2 / 16);
        }

        return (c === 'x' ? r : r & 0x3 | 0x8).toString(16);
      });
    };

    _this.state = {
      cardId: generateUUID(),
      cardHeight: 0,
      lineHeightDivider: _this.props.isLastCard ? 2 : 1
    };
    return _this;
  }

  _createClass(Timeline, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var cardElement = document.getElementById(this.state.cardId);
      var padding = 2 * parseFloat(getComputedStyle(cardElement).fontSize);
      var cardHeight = (cardElement.clientHeight + padding) / this.state.lineHeightDivider;
      this.setState({
        cardHeight: cardHeight
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          direction = _this$props.direction,
          icon = _this$props.icon,
          title = _this$props.title,
          time = _this$props.time,
          description = _this$props.description,
          tags = _this$props.tags,
          labelColor = _this$props.labelColor,
          _this$props$lineColor = _this$props.lineColor,
          lineColor = _this$props$lineColor === void 0 ? 'grey' : _this$props$lineColor,
          _this$props$color = _this$props.color,
          color = _this$props$color === void 0 ? 'grey' : _this$props$color;
      var textAlign = direction === 'left' ? 'right' : 'left';

      var card = /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Card, {
        id: this.state.cardId,
        fluid: true,
        raised: true,
        color: color
      }, /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Card.Content, null, /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Label, {
        pointing: textAlign,
        color: labelColor,
        attached: "top",
        style: {
          marginLeft: '0'
        }
      }, time), /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Card.Header, null, title), /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Card.Description, null, description), tags && /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Divider, null), /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Label.Group, {
        color: color
      }, tags.map(function (tag, i) {
        return /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Label, {
          key: i.toString()
        }, tag);
      })))));

      var left = direction === 'left' ? card : '';
      var right = direction === 'right' ? card : '';
      var isMobile = window.innerWidth <= 768;
      var iconSize = isMobile ? 'small' : 'large';
      var height = this.state.cardHeight;
      return /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("div", {
        className: "Timeline-line",
        style: {
          height: height,
          background: lineColor
        }
      }), /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Grid, null, /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Grid.Row, null, /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Grid.Column, {
        width: 2
      }), /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Grid.Column, {
        width: 5
      }, left), /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Grid.Column, {
        width: 2
      }, /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Icon, {
        name: icon,
        size: iconSize,
        color: color,
        inverted: true,
        circular: true,
        style: {
          margin: 'auto',
          boxShadow: "0 0 0 0.1em ".concat(lineColor, " inset")
        }
      })), /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Grid.Column, {
        width: 5
      }, right), /*#__PURE__*/_react["default"].createElement(_semanticUiReact.Grid.Column, {
        width: 2
      }))));
    }
  }]);

  return Timeline;
}(_react.Component);

Timeline.propTypes = {
  direction: _propTypes["default"].string,
  icon: _propTypes["default"].string,
  title: _propTypes["default"].string,
  time: _propTypes["default"].string,
  description: _propTypes["default"].string,
  color: _propTypes["default"].string
};
var _default = Timeline;
exports["default"] = _default;